var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

//This returns all relevant "choosing account to make resume with" information
exports.getAllAccounts = function(callback){
    //select first_name, last_name, resume_name from  resume r
    //join account a on r.account_id = a.account_id
    //order by first_name asc;
    var query = "select first_name, last_name, resume_name from resume r join account a on"
        + " r.account_id = a.account_id order by first_name asc";
    conosle.log(query);
    connection.query(query, function(err, result){
        callback(err, result);
    });
};

//returns all current resumes in the database
exports.getAll = function(callback){
  var query = "select  resume_name, first_name, last_name, email, a.account_id from account a" +
      " join resume r on a.account_id = r.account_id;";
  console.log("query: " + query);
  connection.query(query, function(err, result){
      callback(err, result);
  })
};


exports.accountSchool = function(id, callback){
    var query = "select school_name, s.school_id from school s join account_school a on a.school_id " +
        "= s.school_id where account_id = " + id;
    console.log(query);
    connection.query(query, function(err, result){
        callback(err, result);
    });
};


exports.accountCompany = function(id, callback){
    var query = "select company_name, c.company_id from company c join account_company a on " +
        "a.company_id = c.company_id where account_id  = " + id;
    connection.query(query, function(err, result){
        callback(err, result);
    });
};


exports.accountSkill = function(id, callback){
    var query = "select skill_name, s.skill_id from skill s join account_skill a on a.skill_id = s.skill_id " +
        "where account_id = " + id;
    connection.query(query, function(err, result){
        callback(err, result);
    });
};

//will have multiple callbacks in order to insert into: resume, acc_sch, acc_comp, acc_skill
//note: "resume_id" assigned at for inserted record, will have to be used to insert the rest of records
/*
resumeObj =
{[school_id],
 [company_id],
 [skill_id],
 account_id,
 resume_name}
 */
exports.insert = function(resumeObj, callback){
    //console.log("resume obj: " + resumeObj);
    var query = "insert into resume (account_id, resume_name) values " +
    "(" + resumeObj.account_id + ", '" + resumeObj.resume_name + "');";
    connection.query(query, function(err, res){
        callback(err, res);
    });
};

exports.insertResumeCompany = function(resumeObj, callback){
    var query = "insert into resume_company (resume_id, company_id, date_shared, was_hired) values ";
    if(resumeObj.company_id instanceof Array) {
        for (var i = 0; i < resumeObj.company_id.length; i++) {          //hard coded date_shared, was_hired values
            query += "(" + resumeObj.resume_id + ", " + resumeObj.company_id[i] + ", '" + "2017-01-01 12:00" + "', '" + "true" + "')";
            if ((i + 1) !== resumeObj.company_id.length) {
                query += " , ";
            }
        }
    }else{
        query += "(" + resumeObj.resume_id + ", " + resumeObj.company_id + ", '" + "2017-01-01 12:00" + "', '" + "true" + "')";
    }

    console.log("query: " + query);
    connection.query(query, function(err, res){
        callback(err, res);
    });
};


exports.insertResumeSkill = function(resumeObj, callback){


    var query = "insert into resume_skill (resume_id, skill_id) values ";
    //make these accept the case and still form a query where resumeObj.skill_id is
    //either an array of ints or a single int
    if(resumeObj.skill_id instanceof Array ){
        for (var i = 0; i < resumeObj.skill_id.length; i++){
            query += "(" + resumeObj.resume_id + ", " + resumeObj.skill_id[i] + ")";
            if((i + 1) !== resumeObj.skill_id.length){
                query += " , ";
            }
        }
    }else{
        query += "(" + resumeObj.resume_id + ", " + resumeObj.skill_id + ");";
    }
    console.log("query: " + query);
    connection.query(query, function(err, res){
        callback(err, res);
    });
};


exports.insertResumeSchool = function(resumeObj, callback){
    var query = "insert into resume_school (resume_id, school_id) values ";
    console.log(resumeObj.school_id);
    if(resumeObj.school_id instanceof Array) {
        for (var i = 0; i < resumeObj.school_id.length; i++) {
            query += "(" + resumeObj.resume_id + ", " + resumeObj.school_id[i] + ")";
            if ((i + 1) !== resumeObj.school_id.length) {
                query += " , ";
            }
        }
    }else{
        query += "(" + resumeObj.resume_id + ", " + resumeObj.school_id + ");";
    }
    console.log("query: " + query);
    connection.query(query, function(err, res){
        callback(err, res);
    });
};