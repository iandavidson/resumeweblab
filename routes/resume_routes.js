var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');
var insert = require('../model/universalInsert');   //access with url: localhost3000:/resume/insert

router.get('/add/selectUser', function(req, res){
    account_dal.getAll(function(err, response){
        //console.log("response" + response);
        res.render('resume/resumeSelectUser', {response: response});
    });
});



router.get('/all', function(req, res){
    resume_dal.getAll(function(err, response){
        console.log("response: " + response);
        res.render('resume/resumeViewAll', {response: response});
    });
});


router.post('/update', function(req, res){
    resume_dal.update(function(err, response){
        if(err){
            res.send(err);
        }else{
            res.redirect('resume/resumeViewAll');
        }
    })
})


router.post('/delete/:id', function(req, res){
    resume_dal.delete(req.params.id, function(err, response){
        if(err){
            res.send("did not learn how to delete parent information :)");//   "delete on cascade"
        }else{
            res.redirect('resume/resumeViewAll');
        }
    })
});

router.get('/add/selectUser/:id', function(req, res){
    var id = req.params.id;
    console.log(id);
    resume_dal.accountSchool(id, function(err1, school){
        resume_dal.accountCompany(id, function(err2, company){
           resume_dal.accountSkill(id, function(err3, skill){
               console.log("response1: " + school);
               console.log("response2: " + company);
               console.log("response3: " + skill);
               var obj = {school:school,    //wrapper obj
                          company: company,
                          skill: skill,
                          id: id};
               console.log('object, school: ' + obj.school);
               res.render('resume/resumeCreateForm', {response: obj});
           })
        });
    })
});

router.post('/insert/:id', function(req, res){//gets called to add a new record.

    var obj =   {resume_name: req.body.resume_name,
                account_id: req.params.id,
                company_id: req.body.company_id,
                school_id: req.body.school_id,
                skill_id: req.body.skill_id};

    //defined: [Object object]
    //undefined : UNDEFINED

    console.log("resume_relevant_obj company" + obj.company_id);
    console.log("resume_relevant_obj school"+ obj.school_id);
    console.log("resume_relevant_obj skill" + obj.skill_id);
    //if(obj.skill_id instanceof Array){
    //    console.log("skills correctly id'ed as an array");
    //}

    //going to assume at this point that if any value is null in obj, just throw error
    if((obj.resume_name || obj.account_id || obj.company_id || obj.school_id || obj.skill_id) == null)
        res.send("not enough given information");
    resume_dal.insert(obj, function(err1, result){//seems like at this point the object "obj" has all undefined values after this point.
        //console.log("test for information: "+query);
        if(err1){
            console.log("resume insert broke");
            res.send(err1);
        }else{
            var resume_id = result.insertId;
            var relatedInsertObj = {        //going to pass this object to the rest of the inserts on tables related to resume.
                company_id: obj.company_id,
                school_id: obj.school_id,
                skill_id: obj.skill_id,
                resume_id: resume_id
            };
            console.log("resume insert successful");
            console.log(relatedInsertObj);
            resume_dal.insertResumeSchool(relatedInsertObj, function(err2, res2){
                if(err2){
                    console.log("Res_school insert threw error");
                    res.send(err2);
                }else{
                    resume_dal.insertResumeCompany(relatedInsertObj, function(err3, res3){
                        if(err3){
                            console.log("res_comp");
                            res.send(err3);
                        }else{
                            resume_dal.insertResumeSkill(relatedInsertObj, function(err4, res4){
                                if(err4){
                                    console.log("res_skil insert threw error");
                                    res.send(err4);
                                }else{
                                    console.log("successfully did all inserts");
                                    res.send("insert successful :)");
                                }
                            })
                        }
                    })
                }
            })

        }
    });
});
/*
res.send('submittion successful');
resume_dal.insertResumeCompany
resume_dal.insertResumeSchool
resume_dal.insertResumeSkill
*/
module.exports = router;